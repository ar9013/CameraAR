package com.yue.ar.suite;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;

public class IOSCameraController implements CameraControl {
    
    public void prepareCamera() {
        
    }

    
    public void startPreview() {

    }

   
    public void stopPreview() {

    }

   
    public void takePicture() {

    }

    
    public byte[] getPreviewData() {
        return new byte[0];
    }

   
    public byte[] getPictureData() {
        return new byte[0];
    }

   
    public void startPreviewAsync() {

    }

  
    public void stopPreviewAsync() {

    }

   
    public void prepareCameraAsync() {

    }


   
    public byte[] takePictureAsync(long timeout) {
        return new byte[0];
    }

    
    public void saveAsJpeg(FileHandle jpgfile, Pixmap cameraPixmap) {

    }

   
    public boolean isReady() {
        return false;
    }
}
