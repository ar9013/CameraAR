package com.yue.ar.suite;

import org.opencv.imgproc.Imgproc;
import org.opencv.highgui.Highgui;
public class ARFilter {
    private Mat markerSrc = null;
    private Mat markerGray = null;
    public ARFilter(Context context, final int markerID)throws IOException{
        markerSrc =  Utils.loadResource(context,markerID,  Highgui.CV_LOAD_IMAGE_COLOR);
        Imgproc.cvtColor(markerSrc, markerGray, Imgproc.COLOR_BGR2GRAY);
    }
}
